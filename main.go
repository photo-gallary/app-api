package main

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"net/http"
	"photp-gallary-api/config"
	"photp-gallary-api/google"
	"photp-gallary-api/handlers"
	"photp-gallary-api/middleware"
	"photp-gallary-api/models"
	"strings"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"golang.org/x/oauth2"

	"github.com/gin-gonic/gin"
)

func main() {
	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if conf.Mode == "dev" {
		db.LogMode(true)
	}

	if err = db.AutoMigrate(&models.Gallary{}, &models.User{}, &models.Image{}).Error; err != nil {
		log.Fatal(err)
	}
	//GIN
	r := gin.Default()
	// config := cors.DefaultConfig()
	// config.AllowOrigins = []string{"http://localhost:3000"}
	// config.AddAllowHeaders = []string{"Origin", "Content-Length", "Content-Type", "Authorization"}
	// r.Use(cors.New(config))

	config := cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}
	r.Use(cors.New(config))

	r.Static("/images", "./upload/dest")

	ug := models.CreateUserService(db, conf.HMACKey)
	uh := handlers.CreateUserHandler(ug)

	gg := models.CreateGallaryService(db)
	gh := handlers.CreateGallaryHandler(gg)

	ig := models.CreateImageService(db)
	ih := handlers.CreateImageHandler(ig)

	if conf.Mode != "dev" {
		gin.SetMode(gin.ReleaseMode)
	}

	r.POST("/signup", uh.SignUp)
	r.POST("/signin", uh.SignIn)
	r.GET("/user/:id", uh.GetUsernameByID)
	r.POST("/upload", gh.UploadImage)
	r.GET("/gallaries", gh.FetchAllGallaries)
	r.GET("/gallary/:id/images", ih.FetchImage)

	ggconf := &oauth2.Config{
		ClientID:     google.GoogleClientID,
		ClientSecret: google.GoogleClientSecret,
		Scopes:       []string{"openid", "profile", "email"},
		RedirectURL:  google.GoogleRedirectURL,
		Endpoint: oauth2.Endpoint{
			AuthURL:  google.GoogleAuthorizeURL,
			TokenURL: google.GoogleAuthorizeToken,
		},
	}
	// http://localhost:8080/oauth/google/connect
	r.GET("/oauth/google/connect", func(c *gin.Context) {
		state := google.RandomState()
		url := ggconf.AuthCodeURL("wtf")
		c.SetCookie("state", state, 3600, "/", "localhost", false, true)
		c.Redirect(302, url)
	})

	r.GET("/callback/google", func(c *gin.Context) {
		code := c.Query("code")

		token, err := ggconf.Exchange(c.Request.Context(), code)
		if err != nil {
			c.Status(400)
			return
		}
		res := token.Extra("id_token").(string)

		base64Profile := strings.Split(res, ".")[1]
		byteProfile, _ := base64.URLEncoding.DecodeString(base64Profile)
		// bytes := string(byteProfile)
		type Google struct {
			Iss           string `json:"iss"`
			Azp           string `json:"azp"`
			Aud           string `json:"aud"`
			Sub           string `json:"sub"`
			Email         string `json:"email"`
			EmailVerified string `json:"email_verified"`
			AtHash        string `json:"at_hash"`
			Name          string `json:"name"`
			Picture       string `json:"picture"`
			GivenName     string `json:"given_name"`
			FamilyName    string `json:"family_name"`
			Locale        string `json:"locale"`
			Iat           string `json:"iat"`
			Exp           string `json:"exp"`
		}
		// _ = byteProfile
		google := []Google{}
		json.Unmarshal(byteProfile, &google)
		c.JSON(200, google)
	})

	protectGal := r.Group("/")
	protectGal.Use(cors.New(config), middleware.AuthenticationRequired(ug))
	{
		protectGal.DELETE("/gallary/:id", gh.DeleteGallaryById)
		protectGal.POST("/gallaries", gh.CreateGallary)
		protectGal.PATCH("/gallary/:id", gh.UpdateStatus)
		protectGal.PATCH("/gallary/:id/fav", gh.UpdateFavourite)
		protectGal.PATCH("/gallary/:id/name", gh.UpdateName)
		protectGal.GET("/gallaries/:id", gh.FetchGallary)
		protectGal.GET("/gallary/:id", gh.GetGallaryById)
		protectGal.GET("/sessions", func(c *gin.Context) {
			user, ok := c.Value("user").(*models.User)
			if !ok {
				c.JSON(401, gin.H{
					"message": "invalid token",
				})
			}
			c.JSON(http.StatusOK, user)
		})
		protectGal.POST("/gallary/:id/images", ih.AddImage)
		protectGal.DELETE("/images/:id", ih.DeleteImage)
		protectGal.PATCH("/images/:id", ih.UpdateCaption)

	}

	r.Run()

}
