package middleware

import (
	"fmt"
	"photp-gallary-api/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type AuthoGorm struct {
	db *gorm.DB
}

func CreateAuthService(db *gorm.DB) *AuthoGorm {
	return &AuthoGorm{db}
}

func AuthenticationRequired(ug *models.UserGorm) gin.HandlerFunc {
	return func(c *gin.Context) {

		header := c.GetHeader("Authorization")
		if header == "" {
			fmt.Println("========================================>" + header)
			c.JSON(401, gin.H{
				"message": "null token",
			})
			c.Abort()
			return
		}
		bearer := len("Bearer ")
		token := header[bearer:]

		if token == "" {
			c.JSON(401, gin.H{
				"message": "Invalid token",
			})
			c.Abort()
			return
		}
		user, err := ug.GetDataFromToken(token)
		if err != nil {
			c.JSON(401, gin.H{
				"message": "Invalid sessions",
			})
			c.Abort()
			return
		}
		// _ = user
		// c.Status(http.StatusOK)

		c.Set("user", user)

	}

}
