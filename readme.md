# EXTRA FEATURES

### STATUS


### DONE

- Set gallary to Favourite then fetch gallary
- Show gallary's author
- Show gallary created date
- Image caption
- Big image
- Notification when add gallary, delete gallary, set gallary's privacy, add image, delete image, login fail
- Clear image button on add image
- Search gallary by name
- Gallary preview Image(Gallary cover image)
- Update gallary name
- User profile info
- User prifile image
- Loading gallary card
- Image sort by rows number


 ###  IN PROGRESS
  - Log in with Google
  - Log in with Facebook
  - Share button
  - Update image caption

# Created by @supreme5678

** Mr.Tonnow Sonsan **
**    602115008     **
**  For Study only  **
