package models

import (
	"log"
	"os"
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
)

type GallaryService interface {
	CreateGallary(gallary *Gallary) error
	FetchGallary() error
}
type Gallary struct {
	gorm.Model
	Name      string  `json:"name"`
	Preview   string  `json:"preview"`
	Status    bool    `json:"status`
	Images    []Image `json:"images"`
	UserID    uint    `json:"user_id"`
	Favourite bool    `json:"favourite"`
}

type NewGallary struct {
	Name      string
	Preview   string
	Status    bool
	UserID    uint `json:"user_id"`
	Favourite bool `json:"favourite"`
}

type GallaryGorm struct {
	db *gorm.DB
}

func CreateGallaryService(db *gorm.DB) *GallaryGorm {
	return &GallaryGorm{db}
}

func (gg *GallaryGorm) CreateGallary(gallary NewGallary) (*Gallary, error) {
	newGal := new(Gallary)
	newGal.Name = gallary.Name
	newGal.Preview = gallary.Preview
	newGal.UserID = gallary.UserID
	newGal.Status = false
	newGal.Favourite = false

	// newGal.Preview = "https://via.placeholder.com/260x160.png/000000/ffffff/&text=LoremIpsum"
	if err := gg.db.Create(newGal).Error; err != nil {
		return newGal, err
	}
	return newGal, nil
}

func (gg *GallaryGorm) FetchGallary(id uint) ([]Gallary, error) {
	gal := []Gallary{}
	if err := gg.db.Where("user_id = ?", id).Find(&gal).Error; err != nil {
		return nil, err
	}
	return gal, nil
}

func (gg *GallaryGorm) FetchAllGallaries() ([]Gallary, error) {
	gal := []Gallary{}
	if err := gg.db.Where("status = 1").Find(&gal).Error; err != nil {
		return nil, err
	}
	return gal, nil
}

func (gg *GallaryGorm) GetOneGallary(id uint) ([]Gallary, error) {
	gal := []Gallary{}
	if err := gg.db.Where("id = ?", id).First(&gal).Error; err != nil {
		return nil, err
	}
	return gal, nil
}

func (gg *GallaryGorm) DeleteOneGallary(id uint) error {
	// gals := []Gallary{}
	// if err := gg.db.Delete(gals, "id = ?", id).Error; err != nil {
	// 	return nil, err
	// }
	// return gals, nil

	tx := gg.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		return err
	}
	err := gg.db.Where("gallary_id = ?", id).Delete(&Image{}).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	err = gg.db.Where("id = ?", id).Delete(&Gallary{}).Error
	if err != nil {
		tx.Rollback()
		return err
	}

	strID := strconv.FormatUint(uint64(id), 10)
	if err := os.RemoveAll(filepath.Join("upload", "dest", strID)); err != nil {
		log.Printf("Fail deleting image files: %v\n", err)
	}

	return tx.Commit().Error

}

func (gg *GallaryGorm) UpdateStatus(id uint, gal Gallary) ([]NewGallary, error) {
	gals := []NewGallary{}
	if err := gg.db.Table("gallaries").Where("id = ?", id).Update("status", gal.Status).Error; err != nil {
		return nil, err
	}
	return gals, nil
}

func (gg *GallaryGorm) UpdateFav(id uint, gal Gallary) ([]NewGallary, error) {
	gals := []NewGallary{}
	if err := gg.db.Table("gallaries").Where("id = ?", id).Update("favourite", gal.Favourite).Error; err != nil {
		return nil, err
	}
	return gals, nil
}

func (gg *GallaryGorm) UpdateName(id uint, gal Gallary) ([]NewGallary, error) {
	gals := []NewGallary{}
	if err := gg.db.Table("gallaries").Where("id = ?", id).Update("name", gal.Name).Error; err != nil {
		return nil, err
	}
	return gals, nil
}
