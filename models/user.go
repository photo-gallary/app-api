package models

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"hash"
	"photp-gallary-api/rand"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type UserService interface {
	SignUp(user *User) error
	GetDataFromToken(token string) (*User, error)
}
type User struct {
	gorm.Model
	Email        string `gorm:"unique_index;not null" json:"email"`
	Password     string `json:"password"`
	Token        string `gorm:"unique_index" json:"token"`
	Firstname    string `json:"firstname"`
	Lastname     string `json:"lastname"`
	ProfileImage string `json:"profile_image"`
}

type UserReg struct {
	Email        string `json:"email"`
	Password     string `json:"password"`
	Token        string `gorm:"unique_index" json:"token"`
	Firstname    string `json:"firstname"`
	Lastname     string `json:"lastname"`
	ProfileImage string `json:"profile_image"`
}

type OpenUser struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

type LoginForm struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserGorm struct {
	db   *gorm.DB
	hmac hash.Hash
}

// var hmacKey = "iloveyou3000"

func CreateUserService(db *gorm.DB, key string) *UserGorm {
	mac := hmac.New(sha256.New, []byte(key))
	return &UserGorm{db, mac}
}

func (ug *UserGorm) SignUpUser(reg UserReg) (string, error) {
	user := new(User)
	user.Email = reg.Email
	user.Firstname = reg.Firstname
	user.Lastname = reg.Lastname
	user.ProfileImage = reg.ProfileImage
	cost := 12
	hash, err := bcrypt.GenerateFromPassword([]byte(reg.Password), cost)
	if err != nil {
		return "", err
	}
	user.Password = string(hash)
	token, err := rand.GetRandom()
	if err != nil {
		return "", err
	}

	ug.hmac.Write([]byte(token))
	tkHsh := ug.hmac.Sum(nil)
	ug.hmac.Reset()
	tkStrHs := base64.URLEncoding.EncodeToString(tkHsh)

	user.Token = tkStrHs
	reg.Token = token

	fmt.Println("token ===============>" + token)
	fmt.Println("token hash ===============>" + tkStrHs)
	if err := ug.db.Create(&user).Error; err != nil {
		return "", err
	}
	return token, nil
}

func (ug *UserGorm) Signin(form *LoginForm) (string, error) {
	found := User{}

	err := ug.db.Where("email = ?", form.Email).First(&found).Error
	if err != nil {
		return "", err
	}
	err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(form.Password))
	if err != nil {
		return "Wrong E-mail or Password", err
	}

	token, err := rand.GetRandom()

	ug.hmac.Write([]byte(token))
	tkHsh := ug.hmac.Sum(nil)
	ug.hmac.Reset()
	tkStrHs := base64.URLEncoding.EncodeToString(tkHsh)

	if err != nil {
		return "Please try again", err
	}
	err = ug.db.Model(&User{}).Where("id = ?", found.ID).Update("token", tkStrHs).Error
	if err != nil {
		return "Please try again", err
	}
	found.Token = token
	return token, err

}

func (ug *UserGorm) GetDataFromToken(token string) (*User, error) {
	ug.hmac.Write([]byte(token))
	tkHsh := ug.hmac.Sum(nil)
	ug.hmac.Reset()
	tkStrHs := base64.URLEncoding.EncodeToString(tkHsh)

	user := new(User)
	err := ug.db.Where("token = ?", tkStrHs).First(&user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (ug *UserGorm) GetUsernameByID(id uint) ([]OpenUser, error) {
	user := []OpenUser{}
	if err := ug.db.Table("users").Where("id = ?", id).First(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}
