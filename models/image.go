package models

import (
	"log"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
)

type ImageService interface {
	AddImage(img *Image) error
	FetchImage() error
}
type Image struct {
	gorm.Model
	FileName  string `json:"image"`
	Src       string `json:"src"`
	Caption   string `json:"caption"`
	GallaryID uint   `json:"gallary_id"`
}

type NewImage struct {
	FileName  string `json:"image"`
	Src       string `json:"src"`
	Caption   string `json:"caption"`
	GallaryID uint   `json:"gallary_id"`
}

type ImageGorm struct {
	db *gorm.DB
}

func CreateImageService(db *gorm.DB) *ImageGorm {
	return &ImageGorm{db}
}

// func (ig *ImageGorm) FilePath(gal_id uint) string {
// 	return
// }

func (ig *ImageGorm) AddImage(image NewImage, strID string) (*Image, error) {
	newImg := new(Image)
	newImg.FileName = image.FileName
	newImg.Caption = image.Caption
	newImg.GallaryID = image.GallaryID

	// u := "http://localhost:8080"

	u, err := url.Parse("https://nowornever2.chickenkiller.com")
	if err != nil {
		log.Fatal(err)
	}
	u.Path = path.Join(u.Path, "images", strID, image.FileName)
	s := u.String()
	newImg.Src = s

	if err := ig.db.Create(newImg).Error; err != nil {
		return newImg, err
	}
	return newImg, nil
}

func (ig *ImageGorm) FetchImage(id uint) ([]Image, error) {
	img := []Image{}
	if err := ig.db.Where("gallary_id = ?", id).Find(&img).Error; err != nil {
		return nil, err
	}
	return img, nil
}

func (ig *ImageGorm) GetByID(id uint) (*Image, error) {
	img := new(Image)
	err := ig.db.First(img, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return img, nil
}

func (ig *ImageGorm) Delete(id uint) error {
	img, err := ig.GetByID(id)
	if err != nil {
		return err
	}
	err = os.Remove(filepath.Join("upload", "dest", strconv.FormatUint(uint64(id), 10), img.FileName))
	if err != nil {
		log.Printf("Fail deleting image: %v\n", err)
	}
	return ig.db.Where("id = ?", id).Delete(&Image{}).Error

}

func (ig *ImageGorm) UpdateCaption(id uint, img Image) ([]NewImage, error) {
	imgs := []NewImage{}
	if err := ig.db.Table("images").Where("id = ?", id).Update("caption", img.Caption).Error; err != nil {
		return nil, err
	}
	return imgs, nil
}
