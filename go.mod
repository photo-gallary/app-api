module photp-gallary-api

go 1.14

require (
	github.com/boj/redistore v0.0.0-20180917114910-cd5dcc76aeff // indirect
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/contrib v0.0.0-20191209060500-d6e26eeaa607
	github.com/gin-gonic/gin v1.5.0
	github.com/go-delve/delve v1.4.1
	github.com/gorilla/sessions v1.2.0 // indirect
	github.com/jinzhu/gorm v1.9.12
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
)
