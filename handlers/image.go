package handlers

import (
	"net/http"
	"os"
	"photp-gallary-api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ImageHandler struct {
	ig *models.ImageGorm
}

func CreateImageHandler(ig *models.ImageGorm) *ImageHandler {
	return &ImageHandler{ig}
}

func (ih *ImageHandler) AddImage(c *gin.Context) {
	newImg := models.NewImage{}
	strID := c.Param("id")
	id, err := strconv.Atoi(strID)
	caption := c.PostForm("caption")

	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}

	file := form.File["photo"][0]
	os.MkdirAll("./upload/dest/"+strID, os.ModePerm)
	err = c.SaveUploadedFile(file, "./upload/dest/"+strID+"/"+file.Filename)
	if err != nil {
		c.JSON(501, gin.H{
			"message": err.Error(),
		})
	}

	newImg.FileName = file.Filename
	newImg.Caption = caption
	newImg.GallaryID = uint(id)

	img, err := ih.ig.AddImage(newImg, strID)
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"image": img,
		// "path":  path.Join("localhost:8080", "images", strID, file.Filename),
	})
}

func (ih *ImageHandler) FetchImage(c *gin.Context) {
	strID := c.Param("id")
	id, err := strconv.Atoi(strID)
	img, err := ih.ig.FetchImage(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, img)
}

func (ih *ImageHandler) DeleteImage(c *gin.Context) {
	strID := c.Param("id")
	id, err := strconv.Atoi(strID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}
	if err := ih.ig.Delete(uint(id)); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}
	c.Status(http.StatusOK)
}

func (ih *ImageHandler) UpdateCaption(c *gin.Context) {
	img := models.Image{}
	strID := c.Param("id")
	id, err := strconv.Atoi(strID)
	if err := c.BindJSON(&img); err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}

	updated, err := ih.ig.UpdateCaption(uint(id), img)
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"message": updated,
	})
}
