package handlers

import (
	"net/http"
	"photp-gallary-api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type GallaryHanler struct {
	gg *models.GallaryGorm
}

func CreateGallaryHandler(gg *models.GallaryGorm) *GallaryHanler {
	return &GallaryHanler{gg}
}

func (gh *GallaryHanler) CreateGallary(c *gin.Context) {
	newGal := models.NewGallary{}
	if err := c.BindJSON(&newGal); err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	// c.JSON(http.StatusOK, gin.H{
	// 	"name": newGal.Name,
	// })
	gal, err := gh.gg.CreateGallary(newGal)
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gal)
}

func (gh *GallaryHanler) FetchGallary(c *gin.Context) {
	strID := c.Param("id")
	id, err := strconv.Atoi(strID)
	gal, err := gh.gg.FetchGallary(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gal)
}

func (gh *GallaryHanler) FetchAllGallaries(c *gin.Context) {
	gal, err := gh.gg.FetchAllGallaries()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gal)
}

func (gh *GallaryHanler) GetGallaryById(c *gin.Context) {
	id := c.Param("id")
	converedID, err := strconv.Atoi(id)
	gal, err := gh.gg.GetOneGallary(uint(converedID))
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"message": gal,
	})
}

func (gh *GallaryHanler) DeleteGallaryById(c *gin.Context) {
	id := c.Param("id")
	converedID, err := strconv.Atoi(id)
	err = gh.gg.DeleteOneGallary(uint(converedID))
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	c.Status(http.StatusOK)
}

func (gh *GallaryHanler) UploadImage(c *gin.Context) {
	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	file := form.File["photo"][0]

	err = c.SaveUploadedFile(file, "./upload/"+file.Filename)
	if err != nil {
		c.JSON(501, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"image": form.File["photo"],
	})
}
func (gh *GallaryHanler) UpdateStatus(c *gin.Context) {
	gal := models.Gallary{}
	strId := c.Param("id")
	id, err := strconv.Atoi(strId)
	if err := c.BindJSON(&gal); err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}

	updated, err := gh.gg.UpdateStatus(uint(id), gal)
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"message": updated,
	})
}

func (gh *GallaryHanler) UpdateFavourite(c *gin.Context) {
	gal := models.Gallary{}
	strId := c.Param("id")
	id, err := strconv.Atoi(strId)
	if err := c.BindJSON(&gal); err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}

	updated, err := gh.gg.UpdateFav(uint(id), gal)
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"message": updated,
	})
}

func (gh *GallaryHanler) UpdateName(c *gin.Context) {
	gal := models.Gallary{}
	strId := c.Param("id")
	id, err := strconv.Atoi(strId)
	if err := c.BindJSON(&gal); err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}

	updated, err := gh.gg.UpdateName(uint(id), gal)
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"message": updated,
	})
}
