package handlers

import (
	"net/http"
	"photp-gallary-api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	ug *models.UserGorm
}

func CreateUserHandler(ug *models.UserGorm) *UserHandler {
	return &UserHandler{ug}
}

func (uh *UserHandler) SignUp(c *gin.Context) {

	// header := c.GetHeader("Authorization")
	// token := header[8:]

	reg := models.UserReg{}
	if err := c.BindJSON(&reg); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	tk, err := uh.ug.SignUpUser(reg)
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"username": reg.Email,
		"token":    tk,
	})
}

func (uh *UserHandler) SignIn(c *gin.Context) {
	// email := c.PostForm("email")
	// password := c.PostForm("password")
	// form := models.LoginForm{
	// 	Email:    c.PostForm("email"),
	// 	Password: c.PostForm("password"),
	// }
	login := models.LoginForm{}
	if err := c.BindJSON(&login); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"isLogin": false,
		})
		return
	}

	token, err := uh.ug.Signin(&login)
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"isLogin": true,
		"token":   token,
	})
}

func (uh *UserHandler) GetUsernameByID(c *gin.Context) {
	strID := c.Param("id")
	id, err := strconv.Atoi(strID)
	user, err := uh.ug.GetUsernameByID(uint(id))
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusOK, user)

}
