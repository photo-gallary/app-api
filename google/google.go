package google

import (
	"crypto/rand"
	"encoding/base64"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

const GoogleAuthorizeURL = "https://accounts.google.com/o/oauth2/v2/auth"
const GoogleAuthorizeToken = "https://oauth2.googleapis.com/token"
const GoogleRedirectURL = "http://localhost:8080/callback/google"
const GoogleClientID = "951144390420-t1ti5fq3b140r38j94nlr9s44lfetlm3.apps.googleusercontent.com"
const GoogleClientSecret = "hsGqqe5NsaRqsmg_ZlqgR5_v"

func RandomState() string {
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		panic(err)
	}
	return base64.URLEncoding.EncodeToString(b)
}

func MakeRequest(token, url, body string) string {

	timeout := time.Duration(5 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}
	payload := strings.NewReader(body)
	req, err := http.NewRequest("GET", url, payload)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+token)
	if err != nil {
		panic(err)
	}

	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()

	result, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	return string(result)
}

func GetGoogleProfile(token string) string {
	url := "https://www.googleapis.com/plus/v1/people/me?access_token={" + token + "}"
	body := `{"limit": 1000}`

	return MakeRequest(token, url, body)
}
